<?php
/**
 * @var CleverFiles self
 */
?>

<?php
$a_options = self::_get_options();
$option_name = self::OPTION_NAME;
?>

<div class="wrap" id="<?= $option_name ?>_settings">
    <h2>Lozad Plugin</h2>

    <form method="post" action="options.php">
        <?php settings_fields($option_name) ?>

        <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <?php esc_html_e('Основные настройки', $option_name) ?>
                </th>
                <td>
                    <fieldset>
                        <label for="<?= $option_name ?>_only_guests">
                            <input type="checkbox" name="<?= $option_name ?>[is_enabled]" id="<?= $option_name ?>_only_guests"
                                   value="1" <?php checked('1', $a_options['is_enabled']); ?> />
                            <?php esc_html_e('Активен', $option_name) ?>
                        </label>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    <?php esc_html_e('Настройки парсинга', $option_name) ?>
                </th>
                <td>
                    <fieldset>
                        <label for="<?= $option_name ?>_only_guests">
                            <input type="checkbox" name="<?= $option_name ?>[is_image]" id="<?= $option_name ?>_only_guests"
                                   value="1" <?php checked('1', $a_options['is_image']); ?> />
                            <?php esc_html_e('Парсинг картинок', $option_name) ?>
                        </label>

                        <br/>

                        <label for="<?= $option_name ?>_reset_on_comment">
                            <input type="checkbox" name="<?= $option_name ?>[is_background]"
                                   id="<?= $option_name ?>_reset_on_comment"
                                   value="1" <?php checked('1', $a_options['is_background']); ?> />
                            <?php esc_html_e('Парсинг фоновых изображений', $option_name) ?>
                        </label>

                        <br/>

                        <label for="<?= $option_name ?>_reset_on_comment">
                            <input type="checkbox" name="<?= $option_name ?>[is_iframe]"
                                   id="<?= $option_name ?>_reset_on_comment"
                                   value="1" <?php checked('1', $a_options['is_iframe']); ?> />
                            <?php esc_html_e('Парсинг iframe', $option_name) ?>
                        </label>

                        <br/>

                        <label for="<?= $option_name ?>_reset_on_comment">
                            <input type="checkbox" name="<?= $option_name ?>[is_video]"
                                   id="<?= $option_name ?>_reset_on_comment"
                                   value="1" <?php checked('1', $a_options['is_video']); ?> />
                            <?php esc_html_e('Парсинг video', $option_name) ?>
                        </label>
                    </fieldset>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    <?php submit_button() ?>
                </th>
            </tr>
        </table>
    </form>
</div>