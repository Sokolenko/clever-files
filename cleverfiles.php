<?php
/*
 * Plugin Name: CleverFiles
 * Plugin URI: https://www.notion.so/CF-Full-Stack-WP-Developer-493a1947dbf4479f8614dc16e0d0d0bb#8e522b044f3b4d22b805e7269a112ab4
 * Description: Тестовое задание
 * Version: 1.0.0
 * Author: Eugene Sokolenko
 * Author URI: https://eugene.sokolenko.biz
 */

add_action(
    'plugins_loaded',
    array(
        'CleverFiles',
        'instance'
    )
);

class CleverFiles
{
    private static $options;

    const OPTION_NAME = 'clever_files';

    const IS_ENABLED = 1;
    const IS_IMAGE = 1;
    const IS_BACKGROUND = 1;
    const IS_IFRAME = 1;
    const IS_VIDEO = 1;

    private static $a_map = [
        'is_image' => 'processImages',
        'is_background' => 'processBackground',
        'is_iframe' => 'processIframe',
        'is_video' => 'processVideo'
    ];

    public static function instance()
    {
        new self();
    }

    public static function on_activation()
    {
        /* Multisite & Network */
        if (is_multisite() && !empty($_GET['networkwide'])) {
            $ids = self::_get_blog_ids();

            foreach ($ids as $id) {
                switch_to_blog($id);
                self::_install_backend();
            }

            restore_current_blog();
        } else {
            self::_install_backend();
        }
    }

    private static function _set_default_vars()
    {
        self::$options = self::_get_options();
    }

    private static function _get_blog_ids()
    {
        /* Global */
        global $wpdb;

        return $wpdb->get_col("SELECT blog_id FROM `$wpdb->blogs`");
    }

    private static function _install_backend()
    {
        add_option(
            self::OPTION_NAME,
            array()
        );
    }

    public function __construct()
    {
        self::_set_default_vars();

        add_action(
            'admin_menu',
            array(
                __CLASS__,
                'lozad_add_pages'
            ),
            99
        );
        add_action(
            'admin_init',
            array(
                __CLASS__,
                'register_settings'
            )
        );

        if (self::$options['is_enabled'] and !is_admin()) {
            $this->_lazyLoad();
        }
    }

    private function _lazyLoad()
    {
        require_once 'classes/classLazyloadProcessing.php';
        $o_lazyLoad = new LozadProcessing();

        $a_option = self::_get_options();
        foreach (self::$a_map as $option_name => $process) {
            if ($a_option[$option_name]) {
                add_filter('clever_files', function ($output) use ($o_lazyLoad, $process) {
                    $o_lazyLoad->$process($output);
                    return $output;
                });
            }
        }

        ob_start();
        add_action('shutdown', function() {
            $content = '';
            $levels = ob_get_level();
            for ($i = 0; $i < $levels; $i++) {
                $content .= ob_get_clean();
            }

            echo apply_filters('clever_files', $content);
        }, 0);

        $this->_initScripts();
    }

    private function _initScripts() {
        wp_enqueue_script( 'lozad', plugins_url( '/js/lozad.js', __FILE__ ));
        wp_enqueue_script( 'lozad_init', plugins_url( '/js/initLozad.js', __FILE__), ['jquery']);
    }

    private static function _get_options()
    {
        return wp_parse_args(
            get_option(self::OPTION_NAME),
            array(
                'is_enabled' => self::IS_ENABLED,
                'is_image' => self::IS_IMAGE,
                'is_background' => self::IS_BACKGROUND,
                'is_iframe' => self::IS_IFRAME,
                'is_video' => self::IS_VIDEO,
            )
        );
    }

    public static function register_settings()
    {
        register_setting(
            self::OPTION_NAME,
            self::OPTION_NAME,
            array(
                __CLASS__,
                'validate_options'
            )
        );
    }

    public static function validate_options($data)
    {
        if (empty($data)) {
            return;
        }

        return array(
            'is_enabled' => (int)$data['is_enabled'],
            'is_image' => (int)$data['is_image'],
            'is_background' => (int)$data['is_background'],
            'is_iframe' => (int)$data['is_iframe'],
            'is_video' => (int)$data['is_video'],
        );
    }

    public static function lozad_add_pages()
    {
        add_options_page('Lozad Plugin | Clever Files', 'Lozad Plugin', 8, 'clever-files', array(
            __CLASS__,
            'lozad_options_page'
        ));
    }

    public static function lozad_options_page()
    {
        require_once 'html/setting.php';
    }
}